/* SystemJS module definition */
declare var module: NodeModule;

interface NodeModule {
  id: string;
}

declare interface RTCPeerConnection {
  ondatachannel: (ev: any) => any;

  createDataChannel(channelName: string);
}

declare interface FileInfo {
  fileSize: number;
  fileName: string;
  data?: any;
}

declare interface Room {
  roomName: string;
  numParticipants: number;
  maxParticipants: number;
}
