import {BrowserModule} from '@angular/platform-browser';
import {APP_INITIALIZER, NgModule} from '@angular/core';
import {AppRoutingModule, routingComponents} from './app-routing.module';
import './rxjs-operators';
// Components
import {AppComponent} from './app.component';
import {ChatComponent} from './component/chat/chat.component';
import {RoomsComponent} from './component/rooms/rooms.component';
// Services
import {WebRtcService} from './service/web-rtc.service';
import {DataChannelService} from './service/data-channel.service';
import {PeerToPeerService} from './service/peer-to-peer.service';
import {MediaService} from './service/media.service';
import {RoomService} from './service/room.service';
import {WebsocketService} from './service/websocket.service';

export function appInitializerConfig(mediaService: MediaService) {
  return async () => {
    console.log('App starts..');
    await mediaService.setupVideo();
    console.log('App ready..');
  };
}

@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    ChatComponent,
    RoomsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [
    WebRtcService,
    DataChannelService,
    PeerToPeerService,
    MediaService,
    RoomService,
    WebsocketService,
    {
      provide: APP_INITIALIZER,
      useFactory: appInitializerConfig,
      deps: [MediaService],
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
