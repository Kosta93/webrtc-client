import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CallPageComponent} from './pages/call-page/call-page.component';
import {HomePageComponent} from './pages/home-page/home-page.component';
import {APP_BASE_HREF} from '@angular/common';

@NgModule({
  imports: [
    RouterModule.forRoot([
      {
        path: '',
        component: HomePageComponent
      }, {
        path: 'inCall',
        component: CallPageComponent,
      },
      {
        path: '**',
        component: HomePageComponent
      }
    ], {useHash: false, onSameUrlNavigation: 'ignore'})
  ],
  providers: [
    {provide: APP_BASE_HREF, useValue: '/'}
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

export const routingComponents = [
  HomePageComponent,
  CallPageComponent
];
