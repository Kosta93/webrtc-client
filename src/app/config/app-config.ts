export const SIGNALING_SERVER_URI = 'wss://localhost:8444';
// export const SIGNALING_SERVER_URI = 'wss://webrtc-signaling-server1.herokuapp.com';


export const DATA_CHANNEL_LABELS = {
  CHAT: 'chat_data',
  FILE_TRANSFER: 'file_transfer'
};

export const DATA_CHANNEL_MESSAGE_TYPES = {
  CHAT_MESSAGE: 'chat_message',
  FILE_INFO: 'file_info'
};
