import {Component, OnInit} from '@angular/core';
import {RoomService} from '../../service/room.service';
import {WebRtcService} from '../../service/web-rtc.service';

@Component({
  selector: 'app-rooms',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.scss']
})
export class RoomsComponent implements OnInit {

  constructor(public roomService: RoomService,
              private webRtcService: WebRtcService) {
  }

  ngOnInit() {
  }

  public joinRoom(roomName) {
    this.webRtcService.joinRoom(roomName);
  }

  public trackByFn(index, room) {
    console.log(index);
    return room.roomName;
  }
}
