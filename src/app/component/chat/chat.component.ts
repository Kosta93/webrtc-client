import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {DataChannelService} from '../../service/data-channel.service';
import {DATA_CHANNEL_LABELS, DATA_CHANNEL_MESSAGE_TYPES} from '../../config/app-config';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {

  @ViewChild('inputEl') inputEl: ElementRef;
  @ViewChild('chatEl') chatEl: ElementRef;
  @ViewChild('fileInput') fileInput: ElementRef;

  private progressBarId: string;

  constructor(private dataChannelService: DataChannelService) {
  }

  ngOnInit() {
    this.dataChannelService.messageStream$.subscribe((msg) => {
      console.log('Message received: ', msg);
      this.chatEl.nativeElement.insertAdjacentHTML('beforeend', `<div class="remote-text">${msg}</div>`);
    });

    this.dataChannelService.fileEvent.subscribe((fileInfo: FileInfo) => {
      console.log('Download completed');
      const progressBar = document.getElementById(this.progressBarId);
      const htmlAnchorElement = document.createElement('a');
      htmlAnchorElement.className = 'download-link';
      htmlAnchorElement.href = fileInfo.data;
      htmlAnchorElement.download = fileInfo.fileName;
      htmlAnchorElement.text = fileInfo.fileName;
      progressBar.parentElement.replaceChild(htmlAnchorElement, progressBar);

      this.progressBarId = null;
    });

    this.dataChannelService.sendProgressEvent.subscribe((progressEvent) => {
      if (progressEvent.fileProgress <= progressEvent.fileSize) {
        if (!!this.progressBarId) {
          const progressBar = document.getElementById(this.progressBarId) as HTMLProgressElement;
          progressBar.value = progressEvent.fileProgress;
          if (progressEvent.fileProgress === progressEvent.fileSize) {
            this.progressBarId = null;
          }
        } else {
          const progressElement = document.createElement('progress');
          this.progressBarId = Math.random().toString(36).substr(2, 5);
          progressElement.id = this.progressBarId;
          progressElement.max = progressEvent.fileSize;
          progressElement.value = progressEvent.fileProgress;
          this.chatEl.nativeElement.insertAdjacentHTML('beforeend', progressElement.outerHTML + `<br/>`);
        }
      }
    });

    this.dataChannelService.receiveProgressEvent.subscribe((progressEvent) => {
      if (progressEvent.fileProgress <= progressEvent.fileSize) {
        if (!!this.progressBarId) {
          const progressBar = document.getElementById(this.progressBarId) as HTMLProgressElement;
          progressBar.value = progressEvent.fileProgress;
        } else {
          const progressElement = document.createElement('progress');
          this.progressBarId = Math.random().toString(36).substr(2, 5);
          progressElement.id = this.progressBarId;
          progressElement.max = progressEvent.fileSize;
          progressElement.value = progressEvent.fileProgress;
          this.chatEl.nativeElement.insertAdjacentHTML('beforeend', progressElement.outerHTML);
        }
      }
    });
  }

  keyPressedHandler(event: KeyboardEvent) {
    if (event.keyCode === 13 && !event.shiftKey) {
      event.preventDefault();
      const input = this.inputEl.nativeElement as HTMLTextAreaElement;
      if (input.value.length === 0 || !input.value.trim()) {
        input.value = '';
        return;
      }
      this.dataChannelService.sendDataMessage(DATA_CHANNEL_LABELS.CHAT, JSON.stringify({
        type: DATA_CHANNEL_MESSAGE_TYPES.CHAT_MESSAGE,
        value: input.value
      }));

      this.chatEl.nativeElement.insertAdjacentHTML('beforeend', `<div class="local-text">${input.value}</div>`);

      input.value = '';
    }
  }

  onDrop(event: DragEvent) {
    event.preventDefault();
    event.stopPropagation();
    const file = event.dataTransfer.files[0];
    this.prepareForFileSend(file);
  }

  onFileInput(fileInput: HTMLInputElement) {
    const file = fileInput.files[0];
    this.prepareForFileSend(file);
  }

  private prepareForFileSend(file) {
    if (!!file) {
      console.log(file);
      this.dataChannelService.sendDataMessage(DATA_CHANNEL_LABELS.CHAT, JSON.stringify({
        type: DATA_CHANNEL_MESSAGE_TYPES.FILE_INFO,
        value: {
          fileName: file.name,
          fileSize: file.size
        }
      }));
      this.dataChannelService.sendFileData(file);
    }
  }
}
