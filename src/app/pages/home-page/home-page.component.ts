import {Component, OnInit} from '@angular/core';
import {WebRtcService} from '../../service/web-rtc.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  constructor(private webRtcService: WebRtcService) {
  }

  ngOnInit() {
  }

  createRoom(roomName: string) {
    if (!(roomName.length === 0 || !roomName.trim())) {
      this.webRtcService.joinRoom(roomName);
    }
  }
}
