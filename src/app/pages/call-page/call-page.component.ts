import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {MediaService} from '../../service/media.service';
import {WebRtcService} from '../../service/web-rtc.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './call-page.component.html',
  styleUrls: ['./call-page.component.scss']
})
export class CallPageComponent implements OnInit {

  @ViewChild('localVideo') localVideo: ElementRef;

  @ViewChild('remoteVideo') remoteVideo: ElementRef;

  @ViewChild('micCanvas') micCanvas: ElementRef;

  constructor(public mediaService: MediaService, private webRtcService: WebRtcService) {
  }

  ngOnInit() {
    const canvasContext = (this.micCanvas.nativeElement as HTMLCanvasElement).getContext('2d');

    this.mediaService.localStream$.filter(stream => !!stream).subscribe(localStream => {
      // console.log(localStream);
      this.connectStreamToSrc(localStream, this.localVideo.nativeElement);
    });

    this.mediaService.remoteStream$.filter(stream => !!stream).subscribe(remoteStream => {
      console.log('Remote stream added..');
      this.connectStreamToSrc(remoteStream, this.remoteVideo.nativeElement);
    });

    this.mediaService.averageSoundLevel.subscribe(averageValue => {
      canvasContext.clearRect(0, 0, 384, 20);
      canvasContext.fillStyle = 'red';
      canvasContext.fillRect(0, 0, averageValue, 20);
    });
  }

  toggleMuteAudio() {
    this.mediaService.muteAudio(!this.mediaService.isAudioMuted());
  }

  togglePauseVideo() {
    this.mediaService.pauseVideo(!this.mediaService.isVideoPaused());
  }

  leaveRoom() {
    this.webRtcService.leaveRoom();
  }

  private async connectStreamToSrc(mediaStream, mediaElement: HTMLMediaElement) {

    mediaElement.srcObject = mediaStream;
    try {
      await mediaElement.play();
    } catch (e) {
    }
  }
}
