import {inject, TestBed} from '@angular/core/testing';

import {MediaService} from './media.service';

describe('AudioLevelService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MediaService]
    });
  });

  it('should be created', inject([MediaService], (service: MediaService) => {
    expect(service).toBeTruthy();
  }));
});
