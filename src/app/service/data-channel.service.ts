import {EventEmitter, Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {DATA_CHANNEL_LABELS, DATA_CHANNEL_MESSAGE_TYPES} from '../config/app-config';
import {PeerToPeerService} from './peer-to-peer.service';

@Injectable()
export class DataChannelService {

  public fileEvent: EventEmitter<FileInfo> = new EventEmitter();
  public sendProgressEvent: EventEmitter<any> = new EventEmitter();
  public receiveProgressEvent: EventEmitter<any> = new EventEmitter();

  private file: FileInfo;
  private dataChannels: Map<string, any> = new Map();
  private messageStream: Subject<string> = new Subject();
  public messageStream$ = this.messageStream.asObservable();
  private receiveBuffer = [];
  private receivedSize = 0;

  constructor(private peerToPeerService: PeerToPeerService) {
  }

  // Set channel created from other peer
  public setNewChannel(dataChannel) {
    this.dataChannels.set(dataChannel.label, dataChannel);
  }

  // Initialize data channel
  public initDataChannel(channelLabel: string) {
    const channel = this.dataChannels.get(channelLabel);
    if (!!channel) {
      channel.onopen = () => {
        console.log(`Data channel state for ${channel.label} is: ${channel.readyState}`);
        console.log(this.dataChannels.get(channelLabel));
      };

      channel.onerror = function (error) {
        console.log('Data Channel Error:', error);
      };

      channel.onclose = () => {
        console.log('Data channel close..');
      };

      channel.onmessage = (event) => {
        // console.log(event.data);
        if (channel.label === DATA_CHANNEL_LABELS.CHAT) {
          try {
            const msg = JSON.parse(event.data);
            if (msg.type === DATA_CHANNEL_MESSAGE_TYPES.CHAT_MESSAGE) {
              this.messageStream.next(msg.value);
            } else if (msg.type === DATA_CHANNEL_MESSAGE_TYPES.FILE_INFO) {
              this.file = msg.value;
              console.log(this.file);
            }
          } catch (e) {
            console.error(e);
          }
        } else if (channel.label === DATA_CHANNEL_LABELS.FILE_TRANSFER) {
          this.handleReceiveFile(event.data);
        }
      };

    } else {
      throw new Error(`Data channel with label ${channelLabel} does not exists`);
    }
  }

  public createNewDataChannel(pc: RTCPeerConnection, channelLabel: string) {
    const channel = pc.createDataChannel(channelLabel);
    this.setNewChannel(channel);
    this.initDataChannel(channel.label);
  }

  public sendDataMessage(channelLabel: string, data: any) {
    const channel = this.dataChannels.get(channelLabel);
    if (channel && channel.readyState === 'open') {
      this.dataChannels.get(channelLabel).send(data);
    }
  }

  public sendFileData(file) {
    this.createNewDataChannel(this.peerToPeerService.peerConnection, DATA_CHANNEL_LABELS.FILE_TRANSFER);

    if (file.size === 0) {
      this.closeDataChannel(DATA_CHANNEL_LABELS.FILE_TRANSFER);
      console.log('File is empty');
      return;
    }

    const chunkSize = 16384;
    const sliceFile = (offset) => {
      const reader = new FileReader();

      reader.onload = (e: any) => {
        this.sendDataMessage(DATA_CHANNEL_LABELS.FILE_TRANSFER, e.target.result);
        // console.log(e.target.result);
        if (file.size > offset + e.target.result.byteLength) {
          setTimeout(sliceFile, 0, offset + chunkSize);
        }
        this.sendProgressEvent.next({
          fileSize: file.size,
          fileProgress: offset + e.target.result.byteLength
        });
      };

      const slice = file.slice(offset, offset + chunkSize);
      reader.readAsArrayBuffer(slice);
    };

    setTimeout(sliceFile, 1000, 0);
  }

  private closeDataChannel(channelLabel: string) {
    const channel = this.dataChannels.get(channelLabel);
    if (!!channel) {
      channel.close();
    }
  }

  private handleReceiveFile(fileData) {
    this.receiveBuffer.push(fileData);
    this.receivedSize += fileData.byteLength;

    this.receiveProgressEvent.next({
      fileSize: this.file.fileSize,
      fileProgress: this.receivedSize
    });

    if (this.receivedSize === this.file.fileSize) {
      const receivedBlob = new Blob(this.receiveBuffer);

      this.closeDataChannel(DATA_CHANNEL_LABELS.FILE_TRANSFER);
      console.log('File transfer completed..');
      this.fileEvent.next({data: URL.createObjectURL(receivedBlob), fileName: this.file.fileName, fileSize: this.file.fileSize});
      this.resetFileData();
    }
  }

  private resetFileData() {
    this.file = null;
    this.receiveBuffer = [];
    this.receivedSize = 0;
  }
}
