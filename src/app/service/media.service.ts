import {EventEmitter, Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class MediaService {

  public averageSoundLevel: EventEmitter<number> = new EventEmitter();

  // local and remote streams
  private localStream: BehaviorSubject<MediaStream> = new BehaviorSubject(null);
  public localStream$ = this.localStream.asObservable();
  private remoteStream: BehaviorSubject<MediaStream> = new BehaviorSubject(null);
  public remoteStream$ = this.remoteStream.asObservable();

  constructor() {
  }

  // setup stream from the local camera
  public async setupVideo(): Promise<MediaStream> {
    const localStream = this.localStream.getValue();
    if (localStream) {
      return localStream;
    } else {
      await new Promise((resolve, reject) => {
        navigator.getUserMedia({
            audio: true, // request access to local microphone
            video: {width: 640, height: 480} // request access to local camera
          }, (newLocalStream) => {
            console.log('Get user media success..');
            // preview the local camera and microphone stream
            this.localStream.next(newLocalStream);

            this.initializeAudioLevelService();
            resolve(newLocalStream);
          },
          error2 => {
            console.error(error2);
            reject('Stream not received..');
          });
      });
    }
  }


  public initializeAudioLevelService() {
    const localStream = this.localStream.getValue();

    // initialize the local variables and geet access to the microphone
    const audioContext = new AudioContext();
    const analyser = audioContext.createAnalyser();
    const microphone = audioContext.createMediaStreamSource(localStream);

    // assign a script processor to the audio context
    const javaScriptNode = audioContext.createScriptProcessor(2048, 1, 1);
    analyser.smoothingTimeConstant = 0.3;
    analyser.fftSize = 1024;
    microphone.connect(analyser);
    analyser.connect(javaScriptNode);
    javaScriptNode.connect(audioContext.destination);

    // set up an audio data processing function
    javaScriptNode.onaudioprocess = () => {
      const array = new Uint8Array(analyser.frequencyBinCount);
      analyser.getByteFrequencyData(array);
      let values = 0;
      const length = array.length;
      for (let i = 0; i < length; i++) {
        values += array[i];
      }

      // calculate the average sound level value and draw it on the canvas
      const average = values / length;
      this.averageSoundLevel.next(average);
    };
  }

  public muteAudio(mute: boolean) {
    this.localStream.getValue().getAudioTracks().forEach(audioTrack => {
      audioTrack.enabled = !mute;
    });
  }

  public isAudioMuted() {
    return !(this.localStream.getValue().getAudioTracks()[0].enabled);
  }

  public pauseVideo(pause: boolean) {
    this.localStream.getValue().getVideoTracks().forEach(videoTrack => {
      videoTrack.enabled = !pause;
    });
  }

  public isVideoPaused() {
    return !(this.localStream.getValue().getVideoTracks()[0].enabled);
  }

  public setRemoteStream(mediaStream: MediaStream) {
    this.remoteStream.next(mediaStream);
  }
}
