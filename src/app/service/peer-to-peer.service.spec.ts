import { TestBed, inject } from '@angular/core/testing';

import { PeerToPeerService } from './peer-to-peer.service';

describe('PeerToPeerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PeerToPeerService]
    });
  });

  it('should be created', inject([PeerToPeerService], (service: PeerToPeerService) => {
    expect(service).toBeTruthy();
  }));
});
