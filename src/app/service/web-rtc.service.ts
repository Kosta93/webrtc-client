import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {Router} from '@angular/router';
import {DATA_CHANNEL_LABELS} from '../config/app-config';
import {DataChannelService} from './data-channel.service';
import {PeerToPeerService} from './peer-to-peer.service';
import {MediaService} from './media.service';
import {WebsocketService} from './websocket.service';
import {RoomService} from './room.service';

@Injectable()
export class WebRtcService {

  private rtcConfiguration: RTCConfiguration = {
    iceServers: [
      {urls: 'stun:' + environment.stunServerUri}, {urls: 'stun:' + environment.stunServerUri2}
    ]
  };

  constructor(private router: Router,
              private dataChannelService: DataChannelService,
              private peerToPeerService: PeerToPeerService,
              private mediaService: MediaService,
              private websocketService: WebsocketService,
              private roomService: RoomService) {
    this.websocketService.init(this.signalHandler.bind(this));
  }

  public async joinRoom(roomToken: string) {
    await this.init();

    // send the room token to the signaling server
    this.peerToPeerService.callToken = roomToken;
    this.websocketService.sendMessage({
      type: 'callee_arrived',
      token: this.peerToPeerService.callToken
    });

    this.router.navigate(['inCall'], {skipLocationChange: true});
  }

  public async leaveRoom() {
    this.websocketService.sendMessage({
      type: 'callee_leaved',
      token: this.peerToPeerService.callToken
    });
    await this.init();

    this.router.navigate(['']);
  }

  private async init() {
    // create the WebRTC peer connection object
    if (this.peerToPeerService.peerConnection) {
      this.peerToPeerService.peerConnection.close();
      this.peerToPeerService.peerConnection = null;
    }
    this.peerToPeerService.peerConnection = new RTCPeerConnection(this.rtcConfiguration);

    try {
      const localStream = await this.mediaService.setupVideo();
      // add local stream to peer_connection ready to be sent to the remote peer
      this.peerToPeerService.peerConnection.addStream(localStream);

      // generic handler that sends any ice candidates to the other peer
      this.peerToPeerService.peerConnection.onicecandidate = (iceEvent) => {
        if (this.peerToPeerService.peerConnection.iceGatheringState === 'complete' && !iceEvent.candidate) {
          console.log('Ice gathering completed..');
        }
        if (iceEvent.candidate) {
          this.websocketService.sendMessage({
            type: 'new_ice_candidate',
            label: iceEvent.candidate.sdpMLineIndex,
            id: iceEvent.candidate.sdpMid,
            candidate: iceEvent.candidate.candidate,
            token: this.peerToPeerService.callToken
          });
        }
      };

      // display remote video streams when they arrive
      this.peerToPeerService.peerConnection.onaddstream = (event) => {
        this.mediaService.setRemoteStream(event.stream);
      };

      this.peerToPeerService.peerConnection.ondatachannel = (event) => {
        console.log('Received data channel create request: ', event.channel);
        this.dataChannelService.setNewChannel(event.channel);
        this.dataChannelService.initDataChannel(event.channel.label);
      };
    } catch (e) {
      console.log(e);
    }
  }

  // generic signal handler
  private async signalHandler(event) {
    const signal = JSON.parse(atob(event.data));
    switch (signal.type) {
      case 'callee_arrived':
        this.dataChannelService.createNewDataChannel(this.peerToPeerService.peerConnection, DATA_CHANNEL_LABELS.CHAT);
        this.peerToPeerService.peerConnection.createOffer(this.newDescriptionCreated.bind(this), error2 => console.error(error2));
        break;
      case 'callee_leaved':
        await this.init();
        break;
      case 'new_ice_candidate':
        this.handleIceCandidateSignal(signal);
        break;
      case 'new_description':
        // handle new description if the type is offer create new answer
        this.peerToPeerService.peerConnection.setRemoteDescription(new RTCSessionDescription(signal.sdp), () => {
          if (this.peerToPeerService.peerConnection.remoteDescription.type === 'offer') {
            this.peerToPeerService.peerConnection.createAnswer(this.newDescriptionCreated.bind(this), error2 => console.error(error2));
          }
        });
        break;
      case 'rooms':
        this.roomService.roomsUpdate(signal);
        break;
      default:
        console.log('custom signal type: ', signal.type);
    }
  }

  // handler to process new descriptions
  private newDescriptionCreated(description) {
    this.peerToPeerService.peerConnection.setLocalDescription(description, () => {
      this.websocketService.sendMessage({
        token: this.peerToPeerService.callToken,
        type: 'new_description',
        sdp: description
      });
    }, error2 => {
      console.error(error2);
    });
  }

  private handleIceCandidateSignal(signal) {
    const candidate = new RTCIceCandidate({
      sdpMLineIndex: signal.label,
      candidate: signal.candidate
    });
    this.peerToPeerService.peerConnection.addIceCandidate(candidate, () => {
    }, error2 => console.log(error2));
  }
}
