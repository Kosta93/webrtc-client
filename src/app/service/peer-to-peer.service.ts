import {Injectable} from '@angular/core';

@Injectable()
export class PeerToPeerService {

  public callToken: string;

  public peerConnection: RTCPeerConnection;

  constructor() {
  }

}
