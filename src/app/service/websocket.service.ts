import {Injectable} from '@angular/core';
import {SIGNALING_SERVER_URI} from '../config/app-config';
import {PeerToPeerService} from './peer-to-peer.service';

@Injectable()
export class WebsocketService {

  private signalingServer: WebSocket;

  // holding the connection state with the signaling server
  private isConnected = false;

  constructor(private peerToPeerService: PeerToPeerService) {
  }

  public init(signalHandler: Function) {
    // setup generic connection to the signaling server using the WebSocket API
    this.signalingServer = new WebSocket(SIGNALING_SERVER_URI);

    this.signalingServer.onmessage = (ev => {
      console.log(atob(ev.data));
      signalHandler(ev);
    });

    this.signalingServer.onopen = () => {
      this.isConnected = true;

      // send rooms request
      this.sendMessage({type: 'rooms'});
    };

    this.signalingServer.onclose = () => {
      this.isConnected = false;
      throw new Error('Socket connection is closed..');
    };

    window.onbeforeunload = (() => {
      this.signalingServer.send(JSON.stringify({
        type: 'callee_leaved',
        token: this.peerToPeerService.callToken
      }));
    });
  }

  public sendMessage(msg: any) {
    if (this.isConnected) {
      const json = JSON.stringify(msg);
      this.signalingServer.send(json);
    } else {
      console.error('Socket connection not opened..');
    }
  }
}
