import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class RoomService {

  private rooms: BehaviorSubject<Array<Room>> = new BehaviorSubject([]);
  public rooms$ = this.rooms.asObservable().filter(rooms => rooms.length > 0)
    .map(rooms => {
      rooms.sort((a, b) => {
        return b.numParticipants - a.numParticipants;
      });
      return rooms;
    });

  constructor() {
  }


  public roomsUpdate(unstructuredRooms) {
    const rooms = [];
    Object.keys(unstructuredRooms).filter(type => type !== 'type')
      .forEach(id => {
        const room = unstructuredRooms[id];
        rooms.push(room);
      });

    this.rooms.next(rooms);
  }
}
