import {Component, OnInit} from '@angular/core';
import {WebRtcService} from './service/web-rtc.service';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(private webRTCService: WebRtcService) {
  }

  ngOnInit(): void {
    Observable.fromEvent(window, 'dragover').subscribe((event: DragEvent) => {
      event.stopPropagation();
      event.preventDefault();
    });

    Observable.fromEvent(window, 'drop').subscribe((event: DragEvent) => {
      console.log(event);
      event.preventDefault();
    });
  }
}
